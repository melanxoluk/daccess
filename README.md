# DAccess
## Idiomatic way to access data in Kotlin

TL/DR
```kotlin
fun main(args: Array<String>) {
    val ds = HikariDataSource(HikariConfig().apply {
        jdbcUrl = "jdbc:h2:mem:retail"
    })
    
    data class Product(val id: Long, val name: String, val amount: Int)
    
    val daccess = object : DAccess {
        override val dataSource = ds
        override val database = selectDatabase()
        
        val products = repository<Product>()
    }
    
    val product = Product(1, "temp", 10)
    with(daccess.products) {
        clear()
        select().toList() shouldBe listOf()
        create(product)
        select().toList() shouldBe listOf(product)
        delete(product)
        select().toList() shouldBe listOf()
    }
    
    ds.close()   
}
```
