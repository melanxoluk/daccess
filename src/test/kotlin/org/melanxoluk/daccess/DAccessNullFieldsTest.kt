package org.melanxoluk.daccess

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import java.io.File


class DAccessNullFieldsTest: StringSpec({
    "Data classes with null fields should works" {
        data class Product(val id: Long, val name: String?, val amount: Int?)

        val ds = HikariDataSource(HikariConfig().apply {
            jdbcUrl = "jdbc:h2:mem:retail2"
        })

        val daccess = object : DAccess(ds) {
            val products = repository<Product>()
        }

        daccess.products.apply {
            create(Product(1, null, null))
            val products = select()
            products shouldBe listOf(Product(1, null, null))
        }

        File("retail2").delete()
    }
})