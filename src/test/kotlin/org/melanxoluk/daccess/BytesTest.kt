package org.melanxoluk.daccess

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import java.io.File


class BytesTest: StringSpec({
    "Data classes with bytes fields should works" {
        data class Product(val id: Long, val name: ByteArray)

        val ds = HikariDataSource(HikariConfig().apply {
            jdbcUrl = "jdbc:h2:mem:null_fields"
        })

        val daccess = object : DAccess(ds) {
            val products = repository<Product>()
        }

        val product = Product(1, "temp".toByteArray())
        daccess.products.apply {
            create(product)
            select().first().name shouldBe "temp".toByteArray()
        }
    }
})