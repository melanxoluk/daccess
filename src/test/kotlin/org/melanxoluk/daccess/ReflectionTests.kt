package org.melanxoluk.daccess

import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import kotlin.reflect.full.primaryConstructor
import kotlin.reflect.jvm.jvmErasure

class ReflectionTests : StringSpec({
    "check declared fields order" {
        data class Example(val a: Int, val b: Int, val c: Int)

        val declaredOrder = listOf("a", "b", "c")
        val reflectionOrder = Example::class.java.declaredFields.map { it.name }
        declaredOrder shouldBe reflectionOrder
    }

    "determine KClass of constructor parameter" {
        data class Child(val name: String)
        data class Parent(val child: Child)

        val params = Parent::class.primaryConstructor!!.parameters
        params[0].type.jvmErasure shouldBe Child::class
    }
})

