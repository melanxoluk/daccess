package org.melanxoluk.daccess

import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import org.melanxoluk.daccess.repository.Vals
import org.melanxoluk.daccess.repository.Value


class ValsTest : StringSpec({
    "Vals should be merged correctly" {
        val origin = Vals(mutableListOf(Value("a".name, 1, 1)))
        val modification = Vals(mutableListOf(Value("a".name, 1, 2)))
        val result = origin + modification
        result["a".name].value shouldBe 2
    }
})