package org.melanxoluk.daccess

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec


class DAccessTests : StringSpec({
    "DAccess instance should be instantiable" {
        val ds = HikariDataSource(HikariConfig().apply {
            jdbcUrl = "jdbc:h2:mem:empty"
        })

        val daccess = object : DAccess(ds) {}
        daccess.database.name.toLowerCase() shouldBe "empty"
        ds.close()
    }

    "DAccess instance with repository should be instantiable" {
        data class Product(val id: Long, val name: String, val amount: Int)

        val ds = HikariDataSource(HikariConfig().apply {
            jdbcUrl = "jdbc:h2:mem:empty"
        })

        val daccess = object : DAccess(ds) {
            val products = repository<Product>()
        }

        daccess.products.select().toList() shouldBe listOf()
        ds.close()
    }

    "DAccess repository should works" {
        val ds = HikariDataSource(HikariConfig().apply {
            jdbcUrl = "jdbc:h2:mem:retail"
        })

        data class Product(val id: Long, val name: String, val amount: Int)

        val daccess = object : DAccess(ds) {
            val products = repository<Product>()
        }

        val product = Product(1, "temp", 10)
        with(daccess.products) {
            clear()
            select().toList() shouldBe listOf()
            create(product)
            select().toList() shouldBe listOf(product)
            delete(product)
            select().toList() shouldBe listOf()
        }

        ds.close()
    }
})
