package org.melanxoluk.daccess

import io.kotlintest.specs.StringSpec


data class Child(val id: Long, var parent: Parent)

data class Parent(val id: Long, val children: MutableList<Child>)

fun f() {
    var p = Parent(1, mutableListOf())
    var c1 = Child(1, p)
    var c2 = Child(2, p)
    p.children.add(c1)
    p.children.add(c2)
    c1.parent = p
    c2.parent = p
}

class MappingTests: StringSpec({
    "children should works" {
        f()
        // data class GrandParent(val id: Long, val child: Parent, val children: List<Parent>)
        // val typeInfo = TypeInfo(GrandParent::class)
        // typeInfo.children["child"] shouldBe TypeInfo(Parent::class)
    }
})
