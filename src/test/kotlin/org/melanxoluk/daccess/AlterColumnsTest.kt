package org.melanxoluk.daccess

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec


class AlterColumnsTest : StringSpec({
    "After initialized table new columns should be possible to add" {
        val ds = HikariDataSource(HikariConfig().apply {
            jdbcUrl = "jdbc:h2:mem:alter_columns"
        })

        fun firstInitialization() {
            data class Example(val a: Int, val b: Int)

            val daccess = object : DAccess(ds) {
                val examples = repository<Example>()
            }

            daccess.examples.create(Example(1, 1))
        }

        fun secondInitialization() {
            data class Example(val a: Int, val b: Int, val c: Int?)

            val daccess = object : DAccess(ds) {
                val examples = repository<Example>()
            }

            daccess.examples.select() shouldBe listOf(Example(1, 1, null))
        }

        firstInitialization()
        secondInitialization()
    }
})