create table product(
    id bigserial primary key,
    name varchar(255) not null,
    amount int not null
);