package org.melanxoluk.daccess.database

import kotlin.reflect.KClass

interface CatalogInfo {
    val catalog: String
}

interface TableInfo {
    val table: String
}

interface ColumnInfo {
    val table: String
    val column: String
    val type: String
    val ordinal: Int
}

data class RDBMS(
    val db: Database,
    val catalogsType: KClass<out CatalogInfo>,
    val tablesType: KClass<out TableInfo>,
    val columnsType: KClass<out ColumnInfo>) {

    companion object {
        val H2 = h2
        val POSTGRES = postgres
    }
}
