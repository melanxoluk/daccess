package org.melanxoluk.daccess.database

import kotlin.reflect.KClass


class TableBuilder(var name: String) {
    private val columns = mutableListOf<Column>()
    private val foreignKeys = mutableListOf<Reference>()

    fun int(name: String) = column(name, "int", Int::class)

    fun string(name: String) = column(name, "varchar", String::class)

    fun column(name: String, sqlType: String, klazz: KClass<*>): Column {
        val col = Column(name, sqlType, klazz)
        columns += col
        return col
    }

    fun build() = Table(name, columns, foreignKeys)
}

fun Database.table(name: String, build: TableBuilder.() -> Unit) =
    TableBuilder(name).apply(build).build().apply { tables[name] to this }


class DatabaseBuilder(var name: String) {
    private val tables = mutableListOf<Table>()

    fun table(name: String, build: TableBuilder.() -> Unit): Table {
        val table = TableBuilder(name).apply(build).build()
        tables += table
        return table
    }


    fun build() = Database(name, tables.map { it.name to it }.toMap())
}

fun database(name: String, build: DatabaseBuilder.() -> Unit) =
    DatabaseBuilder(name).apply(build).build()