package org.melanxoluk.daccess.database


fun createTableSql(table: Table) = with(table) {
    """
        |CREATE TABLE $name
        |(
            ${columns.joinToString(",\n") {
                col -> "|   ${col.name} ${col.sqlType}"
            }}
        |);
    """.trimMargin()
}

fun alterColumnSql(table: Table, column: Column, default: Any? = null): String {
    var sql = "ALTER TABLE ${table.name} ADD COLUMN ${column.name} ${column.sqlType}"

    if (default != null) {
        sql += " DEFAULT $default"
    }

    return sql
}
