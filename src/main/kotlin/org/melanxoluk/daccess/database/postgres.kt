package org.melanxoluk.daccess.database

internal class PostgresCatalogInfo(val tableCat: String): CatalogInfo {
    override val catalog = tableCat
}

internal data class PostgresTableInfo(val tableName: String): TableInfo {
    override val table = tableName
}

internal data class PostgresColumnInfo(
    val tableName: String,
    val columnName: String,
    val typeName: String,
    val ordinalPosition: Int): ColumnInfo {
    override val table = tableName
    override val column = columnName
    override val type = typeName
    override val ordinal = ordinalPosition
}

internal val postgres = RDBMS(
    database("") {
        table("catalogs") {
            string("TABLE_CAT")
        }

        table("tables") {
            string("TABLE_NAME")
        }

        table("columns") {
            string("TABLE_NAME")
            string("COLUMN_NAME")
            string("TYPE_NAME")
            int("ORDINAL_POSITION")
        }

        table("imported_keys") {
            string("PKTABLE_NAME")
            string("PKCOLUMN_NAME")
            string("FKTABLE_NAME")
            string("FKCOLUMN_NAME")
        }
    },
    PostgresCatalogInfo::class,
    PostgresTableInfo::class,
    PostgresColumnInfo::class)