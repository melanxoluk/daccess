package org.melanxoluk.daccess.database

import kotlin.reflect.KClass


data class Database(
    val name: String,
    val tables: Map<String, Table>) {
    operator fun get(name: String) = tables.getValue(name)
}

data class Table(
    val name: String,
    val columns: List<Column>,
    val references: List<Reference>) {
    val length: Int = columns.size
    val names = columns.map { it.name }
    val hasId = columns.firstOrNull { it.name == "id" } != null
}

data class Column(
    val name: String,
    val sqlType: String,
    val klass: KClass<*>)

data class Reference(
    val column: Column,
    val rightTable: Table,
    val rightColumn: Column)