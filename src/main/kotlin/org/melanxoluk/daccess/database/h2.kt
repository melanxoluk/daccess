package org.melanxoluk.daccess.database

internal data class H2CatalogInfo(val catalogName: String): CatalogInfo {
    override val catalog = catalogName
}

internal data class H2TableInfo(val tableName: String): TableInfo {
    override val table = tableName
}

internal data class H2ColumnInfo(
    val tableName: String,
    val columnName: String,
    val typeName: String,
    val ordinalPosition: Int): ColumnInfo {
    override val table = tableName
    override val column = columnName
    override val type = typeName
    override val ordinal = ordinalPosition
}

internal val h2 = RDBMS(
    database("") {
        table("catalogs") {
            string("CATALOG_NAME")
        }

        table("tables") {
            string("TABLE_NAME")
        }

        table("columns") {
            string("TABLE_NAME")
            string("COLUMN_NAME")
            string("TYPE_NAME")
            int("ORDINAL_POSITION")
        }

        table("imported_keys") {
            string("PKTABLE_NAME")
            string("PKCOLUMN_NAME")
            string("FKTABLE_NAME")
            string("FKCOLUMN_NAME")
        }
    },
    H2CatalogInfo::class,
    H2TableInfo::class,
    H2ColumnInfo::class)