package org.melanxoluk.daccess.query

import org.melanxoluk.daccess.database.Table


data class Query(
    val targetTable: Table,
    val joinTables: List<Table>)
