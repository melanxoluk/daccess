package org.melanxoluk.daccess.query

import org.melanxoluk.daccess.DAccess
import org.melanxoluk.daccess.Query
import org.melanxoluk.daccess.TypeInfo
import org.melanxoluk.daccess.database.Column
import org.melanxoluk.daccess.database.Table
import org.melanxoluk.daccess.name
import org.melanxoluk.daccess.repository.Vals
import org.melanxoluk.daccess.repository.Value
import java.sql.ResultSet
import kotlin.reflect.KClass


class TypeQuery<T:Any>(
    private val daccess: DAccess,
    private val clazz: KClass<T>): Query<T> {
    private val typeInfo = TypeInfo(clazz)
    private val table inline get() = typeInfo.table

    private class TableAlias(val alias: String, val table: Table)

    private class LeftJoin(
        val leftTable: Table, val leftColumn: Column,
        val rightTable: Table, val rightColumn: Column
    )

    override fun invoke(): List<T> = with(table){
        var aliasIndex = 1

        fun rec(root: TableAlias, children: List<Table>) {
            for (child in children) {
                val childAlias = TableAlias("t${aliasIndex++}", child)
                "left join ${child.name} $childAlias on ${root.alias}"
            }
        }

        val cols = ""
        val joins = ""
        val sql = "select $cols from $name $joins"
        daccess.withConnection { conn ->
            val rs = conn.createStatement().executeQuery(sql)
            //makeVals()
        }
        TODO()
    }

    fun listVals(rs: ResultSet): List<Vals> = with(table) {
        val list = mutableListOf<Vals>()
        while (rs.next()) {
            val vals = Vals()
            for (i in 1..length) {
                val name = names[i - 1].name
                vals[name] = Value(name, i, rs.getObject(name.snake))
            }
            list += vals
        }
        return list
    }

    private fun leftJoin(leftAlias: TableAlias, leftColumn: Column, rightAlias: TableAlias, rightColumn : Column): String {
        val lAlias = leftAlias.alias
        val lCol = leftColumn.name
        val rTable = rightAlias.table.name
        val rAlias = rightAlias.alias
        val rCol = rightColumn.name
        return "left join $rTable $rAlias on $rAlias.$rCol = $lAlias.$lCol"
    }
}
