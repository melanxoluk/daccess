package org.melanxoluk.daccess

import org.melanxoluk.daccess.database.*
import org.melanxoluk.daccess.query.TypeQuery
import org.melanxoluk.daccess.repository.TableRepository
import org.melanxoluk.daccess.repository.TypeRepository
import java.sql.Connection
import javax.sql.DataSource


open class DAccess(val dataSource: DataSource, rdbms: RDBMS = RDBMS.H2) {
    val database = selectDatabase(rdbms)

    fun <T> withConnection(action: (Connection) -> T): T {
        return dataSource.connection.use(action)
    }

    inline fun <reified T: Any> select(): List<T> {
        return repository<T>().select()
    }

    inline fun <reified T: Any> create(instance: T): T {
        return repository<T>().create(instance)
    }

    inline fun <reified T: Any> delete(instance: T) {
        return repository<T>().delete(instance)
    }

    inline fun <reified T: Any> clear() {
        return repository<T>().clear()
    }


    private fun selectDatabase(rdbms: RDBMS): Database {
        val catalogsRep = TableRepository(this, rdbms.db["catalogs"])
        val catalogsType = TypeInfo(rdbms.catalogsType)

        val tablesRep = TableRepository(this, rdbms.db["tables"])
        val tablesType = TypeInfo(rdbms.tablesType)

        val columnsRep = TableRepository(this, rdbms.db["columns"])
        val columnsType = TypeInfo(rdbms.columnsType)

        return withConnection { connection ->
            val meta = connection.metaData
            val catalog = catalogsType.mapVals(catalogsRep.listVals(meta.catalogs).single())

            val tablesRs = meta.getTables(catalog.catalog, null, null, null)
            val tablesVals = tablesRep.listVals(tablesRs)
            val tables = tablesVals.map { tablesType.mapVals(it) }

            val columnsRs = meta.getColumns(catalog.catalog, null, null, null)
            val columnsVals = columnsRep.listVals(columnsRs)
            val columns = columnsVals.map { columnsType.mapVals(it) }.groupBy { it.table }

            database(catalog.catalog) {
                tables.forEach { table ->
                    table(table.table) {
                        columns[table.table]?.forEach { columnInfo ->
                            column(
                                columnInfo.column,
                                columnInfo.type,
                                DAccessMappings[columnInfo.type].klass)
                        }
                    }
                }
            }
        }
    }
}

inline fun <reified T : Any> DAccess.repository(): Repository<T> =
    TypeRepository(this, T::class)

inline fun <reified T : Any> DAccess.query(): Query<T> =
    TypeQuery(this, T::class)
