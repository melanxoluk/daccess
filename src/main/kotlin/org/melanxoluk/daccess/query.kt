package org.melanxoluk.daccess


interface Query<T: Any> {
    operator fun invoke(): List<T>
}
