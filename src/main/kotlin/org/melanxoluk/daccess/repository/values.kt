package org.melanxoluk.daccess.repository

import org.melanxoluk.daccess.Name


data class Value(val name: Name, val ordinal: Int, val value: Any?) {
    override fun toString(): String {
        return "Value(name=${name.snake}, ordinal=$ordinal, value=$value)"
    }
}

class Vals(val values: MutableList<Value> = mutableListOf()): Sequence<Value> {
    private val vals = values.map { it.name to it }.toMap().toMutableMap()
    val rawValues get() = values.map { it.value }

    operator fun get(name: Name) = vals.getValue(name)

    operator fun set(name: Name, value: Value) {
        values += value
        vals[name] = value
    }

    operator fun plus(vals: Vals): Vals {
        return Vals((this.vals + vals.vals).values.toMutableList())
    }

    override operator fun iterator() = values.iterator()

    override fun toString() = values.toString()
}

