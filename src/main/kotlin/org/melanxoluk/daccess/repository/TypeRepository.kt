package org.melanxoluk.daccess.repository

import org.melanxoluk.daccess.*
import org.melanxoluk.daccess.database.Column
import org.melanxoluk.daccess.database.Table
import org.melanxoluk.daccess.database.alterColumnSql
import org.melanxoluk.daccess.database.createTableSql
import org.slf4j.LoggerFactory
import java.sql.Connection
import kotlin.reflect.KClass


class TypeRepository<T : Any>(
    private val daccess: DAccess,
    private val clazz: KClass<T>) : Repository<T> {
    private val typeInfo = TypeInfo(clazz)
    private val table get() = typeInfo.table

    companion object {
        private val log = LoggerFactory.getLogger(TypeRepository::class.java)

        fun createTable(connection: Connection, table: Table) {
            connection.createStatement().execute(createTableSql(table))
        }

        fun addColumn(connection: Connection, table: Table, column: Column, default: Any? = null) {
            connection.createStatement().execute(alterColumnSql(table, column, default))
        }
    }

    private val tableRepository by lazy { TableRepository(daccess, table) }

    init {
        // - if table not exists create by data class definition
        // - else retrieve table definition from database
        // check existing of table and it's fullness, data class should have all columns of table
        val dbTable = daccess.database.tables[table.name]
        if (dbTable == null) {
            daccess.withConnection {
                createTable(it, table)
                log.info("table created $table")
            }
        } else {
            // data type should contains all columns from table while
            assert(dbTable.columns.minus(table.columns).isEmpty())

            val newColumns = table.columns.minus(dbTable.columns)
            if (newColumns.isNotEmpty()) {
                for (newColumn in newColumns) {
                    daccess.withConnection {
                        addColumn(it, table, newColumn)
                        log.info("column created $newColumn in table $table")
                    }
                }
            }
        }
    }


    override fun select(): List<T> {
        return tableRepository.select().map { typeInfo.mapVals(it) }
    }

    override fun create(instance: T): T {
        return typeInfo.mapVals(tableRepository.create(typeInfo.makeVals(instance)))
    }

    override fun delete(instance: T) {
        tableRepository.delete(typeInfo.makeVals(instance))
    }

    override fun clear() {
        tableRepository.clear()
    }
}
