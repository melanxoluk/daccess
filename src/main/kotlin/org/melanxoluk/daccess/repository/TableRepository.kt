package org.melanxoluk.daccess.repository

import org.melanxoluk.daccess.DAccess
import org.melanxoluk.daccess.Repository
import org.melanxoluk.daccess.database.Table
import org.melanxoluk.daccess.name
import java.sql.ResultSet
import java.util.concurrent.atomic.AtomicLong


class TableRepository(val daccess: DAccess, val table: Table): Repository<Vals> {
    companion object {
        private val id = "id".name
    }

    open inner class Insert {
        open fun insert(vals: Vals): Vals = with(table) {
            val cols = names.joinToString(",") { "?" }
            val sql = "insert into $name values($cols)"
            daccess.withConnection { conn ->
                conn.prepareStatement(sql).run {
                    vals.forEach { setObject(it.ordinal, it.value) }
                    executeUpdate()
                }
            }
            vals
        }
    }

    inner class IdInsert: Insert() {
        private val autoId: AtomicLong
        init {
            @Suppress("CAST_NEVER_SUCCEEDS")
            fun id(vals: Vals) = (vals[id] as Number).toLong()
            val maxId = select().maxBy { id(it) }?.let { id(it) }
            autoId = AtomicLong(maxId ?: 0L)
        }

        override fun insert(vals: Vals) = with(table) {
            vals[id] =
                Value(
                    id,
                    1,
                    autoId.incrementAndGet()
                )
            super.insert(vals)
        }
    }

    private val insert = if (table.hasId) IdInsert() else Insert()

    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
    override fun create(vals: Vals) = insert.insert(vals)

    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
    override fun delete(vals: Vals): Unit = with(table) {
        val notNullValues = vals.values.filter { it.value != null }

        val where = notNullValues.joinToString(" and ") {
            "${it.name.snake}=?"
        }

        val sql = "delete from $name where $where"

        daccess.withConnection { conn ->
            conn.prepareStatement(sql).run {
                notNullValues.forEach { setObject(it.ordinal, it.value) }
                executeUpdate()
            }
        }
    }

    override fun clear(): Unit = with(table) {
        val sql = "truncate table $name"
        daccess.withConnection { conn ->
            conn.createStatement().execute(sql)
        }
    }

    override fun select(): List<Vals> = with(table) {
        val cols = names.joinToString(",")
        val sql = "select $cols from $name"
        daccess.withConnection { conn ->
            val s = conn.createStatement()
            val rs = s.executeQuery(sql)
            listVals(rs)
        }
    }

    fun listVals(rs: ResultSet): List<Vals> = with(table) {
        val list = mutableListOf<Vals>()
        while (rs.next()) {
            val vals = Vals()
            for (i in 1..length) {
                val name = names[i - 1].name
                vals[name] = Value(name, i, rs.getObject(name.snake))
            }
            list += vals
        }
        return list
    }
}
