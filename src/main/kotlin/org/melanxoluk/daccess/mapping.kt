package org.melanxoluk.daccess

import org.melanxoluk.daccess.database.Column
import org.melanxoluk.daccess.database.Table
import org.melanxoluk.daccess.repository.Vals
import org.melanxoluk.daccess.repository.Value
import java.math.BigDecimal
import java.sql.ResultSet
import java.sql.Timestamp
import kotlin.reflect.KClass
import kotlin.reflect.KParameter
import kotlin.reflect.KProperty1
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.full.primaryConstructor
import kotlin.reflect.jvm.jvmErasure


data class BaseTypeMapping(val klass: KClass<*>, val sqlType: String)

object DAccessMappings {
    private val baseMappings = arrayOf(
        Boolean::class to "BOOLEAN",
        Int::class to "INT",
        Int::class to "SMALLINT",
        Int::class to "INTEGER",
        Long::class to "BIGINT",
        Double::class to "FLOAT",
        Double::class to "DOUBLE",
        ByteArray::class to "BYTEA",
        BigDecimal::class to "DECIMAL",
        String::class to "VARCHAR",
        Timestamp::class to "DATETIME",
        Timestamp::class to "TIMESTAMP WITH TIME ZONE"
    ).map { BaseTypeMapping(it.first, it.second) }

    private val typeMappings = baseMappings.map { it.klass to it }.toMap()
    private val sqlMappings = baseMappings.map { it.sqlType to it }.toMap()

    operator fun <T : Any> get(type: KClass<T>) = typeMappings.getValue(type)

    operator fun get(sqlType: String) = sqlMappings.getValue(sqlType)

    fun <T : Any> safeGet(type: KClass<T>) = typeMappings[type]
}


data class TypeInfo<T : Any>(
    val clazz: KClass<T>) {
    val simpleName = clazz.simpleName!!
    val constructor = clazz.primaryConstructor!!
    val parameters = constructor.parameters
    val parameterNames = parameters.mapNotNull(KParameter::name)
    val declaredProperties = clazz.declaredMemberProperties.filter { it.name in parameterNames }.map { it.name to it }.toMap()
    val fields = parameterNames.map { it to declaredProperties.getValue(it) }
    val fieldsMap = fields.toMap()
    val children = children(parameters)
    val table = table(simpleName, children, fields)

    companion object {
        fun <T:Any> table(
            simpleName: String,
            children: Map<String, TypeInfo<out Any>>,
            fields: List<Pair<String, KProperty1<T, *>>>): Table {
            return Table(
                simpleName.snake,
                fields.map { column(it.second, children) },
                listOf()
            )
        }

        fun column(
            property: KProperty1<*, *>,
            children: Map<String, TypeInfo<out Any>>): Column {
            val type = property.returnType.jvmErasure
            val baseMapping = DAccessMappings.safeGet(type)
            return if (baseMapping != null) {
                Column(property.name.snake, baseMapping.sqlType, baseMapping.klass)
            } else {
                val childIdType = children.getValue(property.name).fields.first { it.second.name == "id" }.second
                val childIdBaseMapping = DAccessMappings[childIdType.returnType.jvmErasure]
                Column(
                    property.name.snake,
                    childIdBaseMapping.sqlType,
                    childIdBaseMapping.klass)
            }
        }

        fun children(parameters: List<KParameter>): Map<String, TypeInfo<out Any>> {
            return parameters
                .map { it.name!! to it.type.jvmErasure }
                .filter { (_, kClass) -> kClass.isData }
                .map { (name, kClass) -> name to TypeInfo(kClass) }
                .toMap()
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun mapVals(vals: Vals): T {
        val args = parameterNames.map { vals[it.name].value }.toTypedArray()
        return constructor.call(*args)
    }

    fun makeVals(instance: T): Vals {
        return Vals(parameterNames.mapIndexed { i, name ->
            val field = fieldsMap.getValue(name)
            Value(name.name, i + 1, field.call(instance))
        }.toMutableList())
    }
}