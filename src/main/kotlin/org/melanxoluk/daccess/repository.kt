package org.melanxoluk.daccess


interface Repository<T : Any> {
    fun select(): List<T>

    fun create(instance: T): T

    fun delete(instance: T)

    fun clear()
}
