package org.melanxoluk.daccess.utils

import java.sql.ResultSet

object RsNames {
    operator fun invoke(rs: ResultSet): List<String> {
        val names = mutableListOf<String>()
        repeat(rs.metaData.columnCount) { i ->
            names.add(rs.metaData.getColumnName(i + 1))
        }
        return names
    }
}