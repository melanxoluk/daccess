package org.melanxoluk.daccess.utils

import org.melanxoluk.daccess.name
import org.melanxoluk.daccess.repository.Vals
import org.melanxoluk.daccess.repository.Value
import java.sql.ResultSet

object ListVals {
    operator fun invoke(rs: ResultSet): List<Vals> {
        val names = RsNames(rs)
        val list = mutableListOf<Vals>()

        while (rs.next()) {
            val vals = Vals()
            for (i in 1..names.size) {
                val name = names[i - 1].name
                vals[name] = Value(name, i, rs.getObject(name.snake))
            }

            list.add(vals)
        }

        return list
    }
}