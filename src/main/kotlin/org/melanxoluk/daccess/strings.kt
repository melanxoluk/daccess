package org.melanxoluk.daccess


typealias Name = CashEntry

val String.name get() = cashEntry.let { Name(it.snake, it.camel, it.cebab) }
val String.snake get() = cashEntry.snake
val String.camel get() = cashEntry.camel
val String.cebab get() = cashEntry.cebab


/**
 * Example for 'SimpleWord'
 *
 * val entry = CashEntry("SimpleWord", "SIMPLE_WORD", "simple-word")
 * cash["SimpleWord"] == entry
 * cash["SIMPLE_WORD"] == entry
 * cash["simple-word"] == entry
 */
data class CashEntry(
    val snake: String,
    val camel: String,
    val cebab: String
)

private val cash = mutableMapOf<String, CashEntry>()

private fun createEntry(entry: String): CashEntry {
    val words = entry.words
    return CashEntry(
        entry.snakeTransform(words),
        entry.camelTransform(words),
        entry.cebabTransform(words)
    )
}


private val String.cashEntry: CashEntry
    get() = cash.compute(this) { _, e -> e ?: createEntry(this) }!!

private val String.words: List<Pair<Int, Int>>
    get() {
        val words = mutableListOf<Pair<Int, Int>>()

        var wordBegun = false
        var wordStart = 0
        var wordWholeUpper = false
        var wordLettersAmount = 0

        for ((index, ch) in this.withIndex()) {
            // if last letter was an end of word,
            // then new letter is start of new word
            if (!wordBegun) {
                wordBegun = true
                wordStart = index
                wordWholeUpper = ch.isUpperCase()
                wordLettersAmount = 1

                // words always divided by bottom line
            } else if (ch == '_' || ch == '-') {
                wordBegun = false
                wordWholeUpper = false
                wordLettersAmount = 0
                words.add(wordStart to index)

                // determine finish of word
            } else {
                val isUpper = ch.isUpperCase()
                val isLower = ch.isLowerCase()

                // "BcdAbc" - 'A'
                if (isUpper && !wordWholeUpper) {
                    words.add(wordStart to index)
                    wordBegun = true
                    wordStart = index
                    wordWholeUpper = true
                    wordLettersAmount = 1

                    // "ABC"
                } else if (isUpper && wordWholeUpper) {
                    wordLettersAmount++

                    // "ABilling" - 'i'
                } else if (isLower && wordWholeUpper && wordLettersAmount > 1) {
                    words.add(wordStart to index - 1)

                    wordBegun = true
                    wordStart = index - 1
                    wordLettersAmount = 2
                    wordWholeUpper = false

                    // "Abc" - 'b'
                } else if (isLower) {
                    wordLettersAmount++
                    wordWholeUpper = false
                }
            }
        }

        // add last word
        if (wordLettersAmount != 0) {
            words.add(wordStart to length)
        }

        return words
    }

/**
 * Transforms string to snake case presentation. Divide words by first capital letter
 * or take continuous sequence of upper letters.
 *
 * Examples:
 * - ADMBillingEXAMPLE - ADM_BILLING_EXAMPLE
 * - AdmBillingExample - ADM_BILLING_EXAMPLE
 * - ADM_BILLING_EXAMPLE - ADM_BILLING_EXAMPLE
 * - ADMBILLINGEXAMPLE - ADMBILLINGEXAMPLE
 * - ADm_BILLIng_EXample - A_DM_BILL_ING_E_XAMPLE
 * - aDm_bILLIng_exAmple - A_DM_B_ILL_ING_EX_AMPLE
 */
private fun String.snakeTransform(words: List<Pair<Int, Int>>): String = words.joinToString("_") {
    substring(it.first, it.second).toUpperCase()
}

/**
 * Transforms string to camel case presentation. Divide words by first capital letter
 * or take continuous sequence of upper letters.
 *
 * Examples:
 * - ADMBillingEXAMPLE - AdmBillingExample
 * - AdmBillingExample - AdmBillingExample
 * - ADM_BILLING_EXAMPLE - AdmBillingExample
 * - ADMBILLINGEXAMPLE - Admbillingexample
 * - ADm_BILLIng_EXample - ADmBillIngEXample
 * - aDm_bILLIng_exAmple - ADmBIllIngExAmple
 */
private fun String.camelTransform(words: List<Pair<Int, Int>>): String = words.joinToString("") {
    substring(it.first, it.second).toLowerCase().capitalize()
}

private fun String.cebabTransform(words: List<Pair<Int, Int>>): String = words.joinToString("-") {
    substring(it.first, it.second).toLowerCase()
}
