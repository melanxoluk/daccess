import com.jfrog.bintray.gradle.BintrayExtension
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.gradle.api.tasks.bundling.Jar

plugins {
    kotlin("jvm") version "1.3.50"
    id("com.jfrog.bintray") version "1.8.4"
    `maven-publish`
    maven
}

val daccess = "daccess"
group = "org.melanxoluk"
version = "0.2.10"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.3.50")
    implementation("ch.qos.logback:logback-classic:1.2.3")

    testImplementation("com.zaxxer:HikariCP:3.3.1")
    testImplementation("com.h2database:h2:1.4.199")
    testImplementation("io.kotlintest:kotlintest-runner-junit5:3.3.0")
}

val sourcesJar by tasks.creating(Jar::class) {
    archiveClassifier.set("sources")
    from(kotlin.sourceSets["main"].kotlin)
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }

    withType<GenerateMavenPom> {
        destination = file("$buildDir/libs/$daccess.pom")
    }
}

artifacts {
    archives(sourcesJar)
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform { }
}


val publicationName = daccess

publishing {
    publications.invoke {
        create(publicationName, MavenPublication::class) {
            from(components["java"])
            artifact(sourcesJar)
            afterEvaluate {
                artifactId = daccess
            }
        }
    }
}

bintray {
    user = System.getenv("BINTRAY_USER")
    key = System.getenv("BINTRAY_API")
    publish = true
    setPublications(publicationName)
    pkg(delegateClosureOf<BintrayExtension.PackageConfig> {
        repo = "org.melanxoluk"
        name = daccess
        userOrg = "melanxoluk"
        vcsUrl = "https://gitlab.com/melanxoluk/daccess"
        websiteUrl = vcsUrl
        description = "Idiomatic data access way in Kotlin"
        desc = description
        setLabels("kotlin")
        setLicenses("MIT")
    })
}
