```kotlin
// could have default values?
data class NoIdClass(
    val col1: Int,
    val col2: Long)
    
data class IdClass(
    val id: Int,
    val col1: Int,
    val col2: Long)
    

data class KeyNoIdClass(
    val pk: Key,
    val col1: Int) {
    
    data class Key(
        val col1: Long,
        val col2: Long)
}

data class KeyIdClass(
    val pk: Key,
    val col1: Int) {
    
    data class Key(
        val id: Long,
        val col1: Long)
}
```

### PK is single column
- use default value: `rep.insert(entity)`
- use provided value: `rep.insert(entity, false)`

### PK is composite
- use all default values where column of pk has default value: `rep.insert(entity)`
- use default values for selected columns of pk: `?` 
- use all provided values: `rep.insert(entity, false)`
